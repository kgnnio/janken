package io.gnn;

import java.util.Random;

public class StrategyRandom implements Strategy {
    private Random random;
    private String name;
    private String explanation;

    StrategyRandom() {
        this.name = "Random";
        this.explanation = "randomly return a shape every time.";
        this.random = new Random();
    }

    public String getName() {
        return name;
    }

    public String getExplanation() {
        return explanation;
    }

    public Shape chooseShape() {
        Shape[] shapes = Shape.values();
        return shapes[random.nextInt(shapes.length)];
    }
}
