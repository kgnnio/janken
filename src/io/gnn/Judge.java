package io.gnn;

import java.util.Comparator;
import java.util.HashMap;


public class Judge implements Comparator<Shape> {
    private HashMap<Shape, Shape> shapeBeatsShape;

    Judge() {
        this.shapeBeatsShape = new HashMap<>();
        this.shapeBeatsShape.put(Shape.ROCK, Shape.SCISSORS);
        this.shapeBeatsShape.put(Shape.PAPER, Shape.ROCK);
        this.shapeBeatsShape.put(Shape.SCISSORS, Shape.PAPER);
    }

    public Shape getShapeBeatsShape(Shape shape) {
        return shapeBeatsShape.get(shape);
    }

    @Override
    public int compare(Shape shapeA, Shape shapeB) {
        if (shapeBeatsShape.get(shapeA) == shapeB) {
            return 1;

        } else if (shapeBeatsShape.get(shapeB) == shapeA) {
            return -1;

        } else {
            return 0;

        }
    }
}
