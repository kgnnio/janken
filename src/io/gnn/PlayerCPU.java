package io.gnn;


public class PlayerCPU extends Player {
    private Strategy strategy;

    PlayerCPU(String name, Strategy strategy) {
        super(name);
        this.strategy = strategy;

        System.out.println(name + " is a CPU player.");
    }

    public Shape chooseShape() {
        return strategy.chooseShape();
    }
}
