package io.gnn;

public interface Strategy {
    String getName();
    String getExplanation();
    Shape chooseShape();
}
