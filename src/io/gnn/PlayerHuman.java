package io.gnn;

import java.util.Scanner;

public class PlayerHuman extends Player {
    private Scanner scanner;

    PlayerHuman(String name) {
        super(name);

        scanner = new Scanner(System.in);

        System.out.println(name + " is a human player.");
    }

    public Shape chooseShape() {
        while (true) {
            System.out.println(super.name + "'s choice: (Rock(0), Paper(1), Scissors(2)): ");

            int choice;
            try {
                choice = scanner.nextInt();

                if (choice >= 0 && choice < 3) {
                    Shape[] shapes = Shape.values();
                    return shapes[choice];
                }

            } catch (java.util.InputMismatchException e) {
                System.out.println("Valid input is 0, 1 or 2.");
                scanner.next();

            }
        }
    }
}
