package io.gnn;


public class StrategyJustStone implements Strategy {
    private String name;
    private String explanation;

    StrategyJustStone() {
        this.name = "Just Stone";
        this.explanation = "choose Stone every time.";
    }

    public String getName() {
        return name;
    }

    public String getExplanation() {
        return explanation;
    }

    public Shape chooseShape() {
        return Shape.ROCK;
    }

}
