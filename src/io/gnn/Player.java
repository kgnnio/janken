package io.gnn;

public abstract class Player {
    String name;
    private int score;

    Player(String name) {
        this.name = name;
        this.score = 0;
    }

    public String getName() {
        return name;
    }

    public void incrementScore() {
        score++;
    }

    public int getScore() {
        return score;
    }

    public abstract Shape chooseShape();
}
