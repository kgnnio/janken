package io.gnn;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private Judge judge;
    private Player player1;
    private Player player2;
    private ArrayList<Strategy> strategies;

    Random random;

    public static void main(String[] args) {
        String inputPlayer1Name;
        boolean inputPlayer1IsHuman;
        String inputPlayer2Name;
        boolean inputPlayer2IsHuman;

        if (args.length != 2) {
            System.out.println("Please provide 2 names.");
            System.exit(1);
        }

        inputPlayer1Name = args[0];
        inputPlayer1IsHuman = askUserWhetherPlayerIsHuman(inputPlayer1Name);
        inputPlayer2Name = args[1];
        inputPlayer2IsHuman = askUserWhetherPlayerIsHuman(inputPlayer2Name);

        Game game = new Game(inputPlayer1Name, inputPlayer1IsHuman, inputPlayer2Name, inputPlayer2IsHuman);
        game.play();
    }

    private static boolean askUserWhetherPlayerIsHuman(String name) {
        Scanner scanner = new Scanner(System.in);
        String inputIsHuman;
        while (true) {
            System.out.println("Is " + name + " a human player (yes/no)?");
            inputIsHuman = scanner.nextLine();
            if (inputIsHuman.equalsIgnoreCase("yes")) {
                return true;

            } else if (inputIsHuman.equalsIgnoreCase("no")) {
                return false;

            } else {
                System.out.println("Valid input is \"yes\" or \"no\".");

            }
        }
    }

    private Game(String player1Name, boolean player1IsHuman, String player2Name, boolean player2IsHuman) {
        this.random = new Random();

        this.judge = new Judge();

        strategies = new ArrayList<>();
        strategies.add(new StrategyJustStone());
        strategies.add(new StrategyRandom());

        System.out.println(strategies.get(0).getExplanation());

        if (player1IsHuman) {
            this.player1 = new PlayerHuman(player1Name);

        } else {
            Strategy player1Strategy = strategies.get(random.nextInt(strategies.size()));
            this.player1 = new PlayerCPU(player1Name, player1Strategy);
            System.out.println(player1Name + " has adopted the " + player1Strategy.getName() + " Strategy. " + player1Name + " will " + player1Strategy.getExplanation());

        }

        if (player2IsHuman) {
            this.player2 = new PlayerHuman(player2Name);

        } else {
            Strategy player2Strategy = strategies.get(random.nextInt(strategies.size()));
            this.player2 = new PlayerCPU(player2Name, player2Strategy);
            System.out.println(player2Name + " has adopted the " + player2Strategy.getName() + " Strategy. " + player2Name + " will " + player2Strategy.getExplanation());

        }

        System.out.println("");
    }

    private void play() {
        for (Shape shapeType : Shape.values()) {
            System.out.println(shapeType + " beats " + judge.getShapeBeatsShape(shapeType) + ".");
        }
        System.out.println("");

        for (int i = 0; i < 3; i++) {
            doRound();
            System.out.println("Current score: " + player1.getName() + ": " + player1.getScore() + ", " +
                    player2.getName() + ": " + player2.getScore());
        }
    }

    private void doRound() {
        Shape player1ChosenShape = player1.chooseShape();
        Shape player2ChosenShape = player2.chooseShape();

        System.out.print(player1.getName() + " chose " + player1ChosenShape + ", ");
        System.out.println(player2.getName() + " chose " + player2ChosenShape + ".");

        int judgeVerdict = judge.compare(player1ChosenShape, player2ChosenShape);

        if (judgeVerdict > 0) {
            System.out.println(player1.getName() + " wins.");
            player1.incrementScore();

        } else if (judgeVerdict < 0) {
            System.out.println(player2.getName() + " wins.");
            player2.incrementScore();

        } else {
            System.out.println("Draw.");

        }
    }
}
